using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GString : MonoBehaviour {
	public static string GetPassing(string strDB, string sParser, string eParser,string seperate){
		int sIdx = 0;
		int eIdx = 0; 
		int lastIdx = strDB.LastIndexOf(sParser);
		string rtStr = "";
		
		while(sIdx < lastIdx){
			sIdx = strDB.IndexOf(sParser,sIdx) + sParser.Length;
			eIdx = strDB.IndexOf(eParser,sIdx);
			
			rtStr += strDB.Substring(sIdx,eIdx - sIdx);	
			//insert seperate word between;
			if(sIdx < lastIdx) rtStr += seperate;
			
			//print(sIdx + "  " + eIdx);
			sIdx++;
		}
		return rtStr;
	}
	
	public static string[] GetPassing(string strDB, string sParser, string eParser){
		strDB = "   " + strDB;
		
		int sIdx = 0;
		int eIdx = 0; 
		int lastIdx = strDB.LastIndexOf(sParser);
		//string[] rtStr;
		
		List<string> rtStr = new List<string>();
		
		while(sIdx < lastIdx){
			sIdx = strDB.IndexOf(sParser,sIdx) + sParser.Length;
			eIdx = strDB.IndexOf(eParser,sIdx);
			
			rtStr.Add(strDB.Substring(sIdx,eIdx - sIdx));
			
			//print(sIdx + "  " + eIdx);
			sIdx++;
		}
		
		return rtStr.ToArray();
	}
	
	
	public static void Test(){
		string str = "purchaseSignatureVerifiedEvent. signedData: {\"nonce\":8613213796864703527,\"orders\":[{\"orderId\":\"460304129293253\",\"packageName\":\"net.gamsoft.inapptest\",\"" +
			"productId\":\"consumable01\",\"purchaseTime\":1345107799000,\"purchaseState\":0},{\"orderId\":\"636734413959054\",\"packageName\":\"net.gamsoft.inapptest\",\"" +
			"productId\":\"consumable02\",\"purchaseTime\":1345108248000,\"purchaseState\":1},{\"orderId\":\"556469855243234\",\"packageName\":\"net.gamsoft.inapptest\",\"" +
			"productId\":\"consumable03\",\"purchaseTime\":1345163538000,\"purchaseState\":2}]}";
		
		Debug.Log (GString.GetPassing(str,"\"productId\":\"","\",\"")[0]);
		Debug.Log (GString.GetPassing(str,"\"productId\":\"","\",\"")[1]);
		Debug.Log (GString.GetPassing(str,"\"productId\":\"","\",\"")[2]);
		
		Debug.Log (GString.GetPassing(str,"\"purchaseState\":","}")[0]);
		Debug.Log (GString.GetPassing(str,"\"purchaseState\":","}")[1]);
		Debug.Log (GString.GetPassing(str,"\"purchaseState\":","}")[2]);
		
		
	}
}
