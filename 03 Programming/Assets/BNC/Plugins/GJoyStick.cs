﻿using UnityEngine;
using System.Collections;

public class GJoyStick : MonoBehaviour {

	[System.Serializable]
	public class GAnalogStick{
		public UISprite back;
		public UISprite inStick;

		public float radius ;
		Transform backTransform;
		public Transform backtf {	get{ if( backTransform == null ) backTransform = back.transform; return backTransform; } }
		Transform inStickTransform;
		public Transform inSticktf {	get{ if( inStickTransform == null ) inStickTransform = inStick.transform; return inStickTransform; } }

		public bool IsOutArea( Vector2 pos ){
			if (Vector2.Distance ( back.transform.position, pos) > radius)
				return true;
			else 
				return false;
		}

		public Vector3 CulcurateStickMove(){
			Vector3 moveVector = inSticktf.position - backtf.position;
			float dis = Vector3.Distance (inSticktf.position, backtf.position);
			moveVector = moveVector.normalized * dis;
			return moveVector / radius;
		}

		public void SynchStickToBack(){
			inSticktf.position = backtf.position;
		}
	}

	public Vector3 basePoint;

	public Transform tfCamera;

	public GAnalogStick stick;

	public float pass = 0.02f;

	public bool isUsing = false;

#if UNITY_EDITOR
	public Vector2 editorViewVector;
#endif
	public Vector2 moveVector{ get { 
			if(isStickControling) return stick.CulcurateStickMove (); 
			else return Vector2.zero; 
		} 
	}

//	public Rect boxArea;

	Transform mtf;

	Vector3 touchPos;

	bool isStickControling;
	bool stickComebackSwitch;

	// Use this for initialization
	void Start () {
		mtf = transform;
//		stick.inStick.transform.localPosition = stick.back.transform.localPosition = Vector3.zero;

	}
	
	// Update is called once per frame
	void Update () {
#if UNITY_EDITOR
		editorViewVector = moveVector;
#endif

		if (isStickControling)
			return;

#if UNITY_EDITOR
		if ( stickComebackSwitch) {
#elif UNITY_IPHONE || UNITY_ANDROID
			if ( stickComebackSwitch || Input.touchCount < 1) {
#else 
			if ( stickComebackSwitch) {
#endif
			Vector2 comeback_dir = stick.backtf.position - stick.inSticktf.position;
			stick.inSticktf.Translate( comeback_dir * 0.1f);

			if( comeback_dir.magnitude < pass ){
				stick.SynchStickToBack();
				stickComebackSwitch = false;
				stick.inStick.enabled = false;
			}
		}
	}

	void OnDrawGizmosSelected() {
		if (mtf == null)
			mtf = transform;
		if (tfCamera == null)
			tfCamera = UICamera.currentCamera.transform;

//		Gizmos.color = Color.blue;
//
//		Vector2 pos1 = new Vector2( tfCamera.position.x + boxArea.x , tfCamera.position.y + boxArea.y );
//		Vector2 pos2 = new Vector2( tfCamera.position.x + boxArea.x + boxArea.width , tfCamera.position.y + boxArea.y );
//		Vector2 pos3 = new Vector2( tfCamera.position.x + boxArea.x + boxArea.width , tfCamera.position.y + boxArea.y + boxArea.height );
//		Vector2 pos4 = new Vector2( tfCamera.position.x + boxArea.x , tfCamera.position.y + boxArea.y + boxArea.height );
//
//		Gizmos.DrawLine( pos1, pos2 );
//		Gizmos.DrawLine( pos2, pos3 );
//		Gizmos.DrawLine( pos3, pos4 );
//		Gizmos.DrawLine( pos4, pos1 );

		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere (transform.position, stick.radius);

		Gizmos.color = Color.red;
		Gizmos.DrawLine (stick.backtf.position, stick.inSticktf.position);
		Gizmos.DrawLine (stick.inSticktf.position, touchPos );
		
		touchPos.z = 0f;
		Gizmos.DrawWireSphere (touchPos, 10f * tfCamera.camera.orthographicSize/100f);
	}

	void OnHover(){
//				print ("dkdk");
//				stick.backtf.position = GetNGUITouchPostion( UICamera.lastTouchPosition, 0f);
	}

	void OnPress (bool isDown) {
		if (! isDown) { 
			stickComebackSwitch = true;
			isStickControling = false;
			touchPos = stick.backtf.position;

		} else { 
			stick.backtf.position = GetNGUITouchPostion( UICamera.lastTouchPosition, 0f);
		}
		isUsing = isDown;
	}

	void OnDrag( Vector2 delta )
	{
		MoveStick ();
	}

	void MoveStick(){

		isStickControling = true;

		stick.inStick.enabled = true;

		touchPos = GetNGUITouchPostion( UICamera.lastTouchPosition, 0f);

		if (! stick.IsOutArea (touchPos)) {
			stick.inSticktf.position = touchPos;
		} else {
			Vector2 dir = touchPos - stick.backtf.position;
			
			Vector3 tPos = dir.normalized * stick.radius;
			
			stick.inSticktf.position = stick.backtf.position + tPos;
		}
	}

	Vector3 GetNGUITouchPostion(Vector3 pos, float depth){	
		Ray ray = UICamera.currentCamera.ScreenPointToRay(pos);
		return ray.GetPoint(depth);
	}



}
