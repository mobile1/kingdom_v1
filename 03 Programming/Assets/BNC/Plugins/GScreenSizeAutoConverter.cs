using UnityEngine;
using System.Collections;

[AddComponentMenu("GAM SOFT/GUI/GScreenSizeAutoConverter")] 

[ExecuteInEditMode]
public class GScreenSizeAutoConverter : MonoSingleton<GScreenSizeAutoConverter> {
	public enum Orientation{
		Portrait,Randscape,	
	}
	
	public Camera _2d_Camera;
	
	public bool realTime = true;
	
	public float work_Width = 640f;
	public float work_Height = 960f;
	
	public float screenWidth;
	public float screenHeight;
	
	public float stretchWidth;
	public float stretchHeight;
	
	public float workWnH;
	public float workHnW;
	
	float widthTemp;
	float widthCurTemp;
	
	// Use this for initialization
	void Start () {
		if(_2d_Camera == null) 	_2d_Camera = Camera.main.camera;
			
		ReSize();
	}

	void ReSize(){
		//Screen.SetResolution((int)work_Width,(int)work_Height,true);	
		//return;
		/*
		if(Screen.width > Screen.height){
			stretch = (float)Screen.width / Screen.height;
			current_orientation = GScreenSizeAutoConverter.Orientation.Randscape;
		}else{
			stretch = 	(float)Screen.height / Screen.width;
			current_orientation = GScreenSizeAutoConverter.Orientation.Portrait;
		}
		
		if(Screen.orientation ==){
				transform.localScale = new Vector3(1,stretch/1.5f,1);
		}else{
				transform.localScale = new Vector3(stretch/1.5f,1,1);		
		}
		*/
		
		screenWidth = Screen.width;
		screenHeight = Screen.height;
		
		workWnH = work_Width / work_Height;
		workHnW = work_Height / work_Width;
				
		stretchWidth = (float)Screen.width / Screen.height * (workHnW);
		
		transform.localScale = new Vector3(stretchWidth,1,1);
		
		widthTemp = Screen.width;
		widthCurTemp = Screen.currentResolution.width;
	
	}
	// Update is called once per frame
	void Update () {



		if(realTime && (widthTemp != Screen.width || widthCurTemp != Screen.currentResolution.width)) 
			ReSize();
	}
}
