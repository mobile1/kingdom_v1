using UnityEngine;
using System.Collections;
using System.Collections.Generic;
// NGUI 의 그리드 리스트 컨트롤을 환장한 클래스.
// 데이터를 추가 삭제 기능을 확장 하였다.
public class UIGridExtend : UIGrid {


    // 추가 시킬 프리팹.
    public GameObject itemPrefab;
    // 부모 패널 갱신 시키기 위해 사용 한다.
    public UIPanel panel;
    // 드래그를 허용 할 것인가?.
    public bool useDrag = false;

    UIScrollView dragPanel;
    // 리스트가 간격에 마춰 움직인다.
    public bool snapping = false;
    // 현재 가리키고 있는 커서.
    public int cursor;
    // 추가된 오브젝트 리스트.
    public List<GameObject> list;
    
    
    void Awake()
    {
        panel = NGUITools.FindInParents<UIPanel>(gameObject);
		dragPanel = NGUITools.FindInParents<UIScrollView>(gameObject);
          

        if(useDrag )
        {
			if(panel.GetComponent<UIScrollView>() == null)
            {
				dragPanel = panel.gameObject.AddComponent<UIScrollView>(); 
            }
        }
        else
        {
			
			if(panel.GetComponent<UIScrollView>() != null)
            {
				DestroyImmediate(panel.GetComponent<UIScrollView>());
            }
        }
        
        if(snapping)
        {
            if(GetComponent<UICenterOnChild>() == null){
                gameObject.AddComponent<UICenterOnChild>();  
            }
        }else{
            if(GetComponent<UICenterOnChild>() != null){
                DestroyImmediate(GetComponent<UICenterOnChild>());
            }
        }
		
	
    }

	public void Add(GameObject prefab){
		bool temp = panel.widgetsAreStatic;
		panel.widgetsAreStatic = false;
		
		list.Add(NGUITools.AddChild( gameObject,prefab ));
		list[list.Count - 1].gameObject.name = "" + (list.Count - 1);	
				
		Reposition();
		panel.Refresh();    
		
		if( dragPanel != null ) dragPanel.RestrictWithinBounds(true);
		
		panel.widgetsAreStatic = temp;
		
	}
	
	public void Add()
	{        
		Add ( itemPrefab );
	}  
	
    public void Insert(int index)
    {
        bool temp = panel.widgetsAreStatic;
        panel.widgetsAreStatic = false;
        
		list.Insert(index,NGUITools.AddChild(gameObject,itemPrefab));
		
		for(int i = 0; i < list.Count; i++){
			list[i].name = "" + i;
			list[i].transform.parent = gameObject.transform;		
		}
		
		Reposition();
        panel.Refresh();    
        
        panel.widgetsAreStatic = temp;
    }
	
    public void RemoveAt(int index){
        if(list.Count <= 0) return;
		
		bool bTemp = panel.widgetsAreStatic;
		panel.widgetsAreStatic = false;
		
		//target item position backup
		Vector3 vTemp = list[index].transform.localPosition;
		//target item delete
		NGUITools.DestroyImmediate(list[index]);
		list.RemoveAt( index );
		
		Vector3 vTemp2;
		
		for(int i = index + 1; i < list.Count; i++){
			vTemp2 = list[i].transform.localPosition;
			list[i].transform.localPosition = vTemp;
			vTemp = vTemp2;
		}
		
		dragPanel.RestrictWithinBounds(true);
		
		SpringPanel spring = panel.GetComponent<SpringPanel>();
		
		if(spring){
			spring.target = Vector3.zero;
		}
		
		Reposition();
        panel.Refresh();   
		
		panel.widgetsAreStatic = bTemp;
    }    
	
    public void RemoveFirst()
    {
        RemoveAt(0);
    }

    public void RemoveLast()
    {
        RemoveAt (list.Count - 1);
    }    
	
    public void Clear()
    {
		if(list.Count <= 0) return;
		
		bool bTemp = panel.widgetsAreStatic;
		panel.widgetsAreStatic = false;
		
		for(int index = list.Count - 1; index >= 0 ; index--)
		{
			NGUITools.DestroyImmediate(list[index]);
			list.RemoveAt( index );
		}
		
		SpringPanel spring = panel.GetComponent<SpringPanel>();
		
		if(spring){
			spring.target = Vector3.zero;
		}
		
		SetCursor(0);
		
		Reposition();
       	 	panel.Refresh();   
		
		panel.widgetsAreStatic = bTemp;
    }
        
    public void SetCursor(int index){
        
		if(index != 0){
	        if(index < 0) index = 0;
	        if(index > list.Count - 1) index = list.Count - 1;
		}
        
        cursor = index;
        
        SlideITween(index);
    } 

    public int GetCursor(){
        return cursor;
    }
    public void MoveFirst(){
        SetCursor(0);
    } 
    public void MoveLast(){
        SetCursor(list.Count - 1);
    }
    public void MoveNext(){
        SetCursor(cursor + 1);
    }
    public void MovePre(){
        SetCursor(cursor - 1);
    }
	public void SlideITween(int index){
        Hashtable itweenParam = new Hashtable();
        
        if(arrangement == UIGrid.Arrangement.Horizontal) itweenParam.Add("x", -(index) * cellWidth);
        else itweenParam.Add("y", -(index) * cellHeight);
        
        itweenParam.Add("time", 0.5f);
        itweenParam.Add("islocal", true);
        itweenParam.Add("easetype",iTween.EaseType.easeOutQuad);
        itweenParam.Add("onupdate","SlideSynch");
        itweenParam.Add("onupdatetarget",gameObject);
        iTween.MoveTo (panel.gameObject, itweenParam);
    }
    public void SlideSynch(){
//        Vector4 tV4 = panel.clipRange;
		Vector4 tV4 = panel.baseClipRegion;//.finalClipRegion;//.clipRange;

		if(arrangement == UIGrid.Arrangement.Horizontal)	tV4.x = -panel.transform.localPosition.x;        
		else tV4.y = -panel.transform.localPosition.y;
		
//        panel.clipRange = tV4;      
        panel.baseClipRegion = tV4;      
    }
}
