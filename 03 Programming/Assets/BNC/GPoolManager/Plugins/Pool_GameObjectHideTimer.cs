﻿using UnityEngine;
using System.Collections;
using GPool;

namespace GPool{
	public class Pool_GameObjectHideTimer : MonoBehaviour 
	{
		public float checkTime = 1f;
		
		// Use this for initialization
		void OnEnable () {	
			StartCoroutine(CheckHide ());		
		}
		
		void OnDisable () {
			StopAllCoroutines();
		}
		
		IEnumerator CheckHide()
		{			
			while( true )
			{
				yield return new WaitForSeconds( checkTime );
				MGGameObject.i.HideObject( gameObject );		
				break;			
			}			
		}
		
		public void StopTimer()
		{
			StopAllCoroutines();
		}
	}
}
