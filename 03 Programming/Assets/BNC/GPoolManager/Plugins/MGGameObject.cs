using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GPool{
	
	[System.Serializable]
	public class PoolFolder{
		[HideInInspector]
		public string name;
		public Object folder;
		public GPPrefabInfo[] prefabInfos;
	}
	
	[System.Serializable]	
	public class GPPrefabInfo{		
		[HideInInspector]
		public string name;
		public GameObject prefab;
		public int defaultCreateCount = 1 ;
		
		//Construct
		public GPPrefabInfo(GameObject go){
			prefab = go;
			name = prefab.name + " (" + defaultCreateCount + ")";				
		}
	}
	
	//GPool ==> GPPrefabFolders ==> GPPrefabs ==> createObjects
	public class MGGameObject : MonoSingleton<MGGameObject> {

		//##################################################################################
		// ObjectCollection 
		//
		//
		//##################################################################################
		[System.Serializable]	
		public class GPCollection{
			public Transform parent;
			public List< GameObject > objects = new List< GameObject >();
			public GPPrefabInfo info;
			
			public GPCollection(GPPrefabInfo _info){
				info = _info;
			 	GameObject go = new GameObject(info.name + "s" );
				go.transform.parent = GPool.MGGameObject.i.transform;
				parent = go.transform;
			
				info.defaultCreateCount = Mathf.Max( PlayerPrefs.GetInt(info.name,0), info.defaultCreateCount);

				for ( int i = 0; i < info.defaultCreateCount ; i++){
					AddCreateObject( false , Vector3.zero	 );
				}
			}
			
			
			public GameObject AddCreateObject ( bool isActive, Vector3 pos){
				GameObject go = Instantiate( info.prefab ) as GameObject; //cerate
				
				go.name = info.prefab.name;
				go.transform.parent = parent;
				go.transform.position = pos;													//postioning
				go.gameObject.SetActive( isActive );										//active
//				go.AddComponent<Pool_AutoHideObject>();
				objects.Add ( go ); //add
				
				return go;  //return
			}
			public GameObject AddCreateObject ( bool isActive){
				return AddCreateObject( isActive, Vector3.zero );
			}
			
			public GameObject GetObject( Vector3 pos ){
				//미 사용중인 객체를 찾아 활성화 시키고 위치를 조정후 return
				for( int i = 0; i < objects.Count ; i++){
					
					if ( ! objects[i].activeSelf ){ 	//search
						objects[ i ].SetActive(true); 
//						objects[ i ].renderer.enabled = true;
//						objects[ i ].collider.enabled = true;
						objects[ i ].transform.position = pos; 					//positioning
//						objects[ i ].transform.localScale = Vector3.one;
						return objects[ i ].gameObject;
					}				
				}

				PlayerPrefs.SetInt(info.name,objects.Count + 1);
				return AddCreateObject( true, pos );
			}
			
		}
		public PoolFolder[] folders;
//		public List<GPCollection> collections_debug = new List<GPCollection>();
		public Dictionary<string, GPCollection> collections = new Dictionary<string, GPCollection>();
	
		bool isInit = false;

		// test
		public void CreateObject(string key){
			PlayerPrefs.SetInt(collections[key].info.name,collections[key].objects.Count + 1);
			collections[key].AddCreateObject(true,Vector3.zero);
		}

		// Use this for initialization
		void Awake () {				
			CreateCollection();
			isInit = true;
		}
				
		public void RenamePrefabInfos(){
			for(int fid = 0; fid < folders.Length; fid++){
				for(int i = 0; i < folders[fid].prefabInfos.Length; i++){
					folders[fid].prefabInfos[i].name = folders[fid].prefabInfos[i].prefab.name + " (" + folders[fid].prefabInfos[i].defaultCreateCount + ")";
				}
			}
		}
		
		/// <summary>
		/// Renames this instance.
		/// 프리펩의 이름에 다가 현재 생성한 오브젝트의 갯수를 표기한다.
		/// </summary>
		public void LoadObjectCount(){
			for(int fid = 0; fid < folders.Length; fid++){
				for(int i = 0; i < folders[fid].prefabInfos.Length; i++){				
					folders[fid].prefabInfos[i].defaultCreateCount = PlayerPrefs.GetInt(folders[fid].prefabInfos[i].name,1);
				}
			}
		}
		
		public void SaveObjectCount(){
			for(int fid = 0; fid < folders.Length; fid++){
				for(int i = 0; i < folders[fid].prefabInfos.Length; i++){				
					PlayerPrefs.SetInt(folders[fid].prefabInfos[i].name, folders[fid].prefabInfos[i].defaultCreateCount);
				}
			}
		}
		
		public void CreateCollection(){
			
//			collections_debug.Clear();
			
			collections.Clear();
			
			for(int fid = 0; fid < folders.Length; fid++){
				foreach ( GPPrefabInfo info in folders[fid].prefabInfos ){
					GPCollection collection = new GPCollection( info );	
					
	//				collections_debug.Add( collection );
					collections.Add(  info.prefab.name, collection );
				}
			}
		}
		
		/// <summary>
		/// Gets the object.
		/// 외부 클래스에서 본 함수를 통해 미리 생성된 객체를 가져다가 사용한다.
		/// </summary>
		/// <returns>
		/// The object.
		/// </returns>
		/// <param name='key'>
		/// Key.
		/// </param>
		/// <param name='pos'>
		/// Position.
		/// </param>
		public GameObject GetObject( string key ,Vector3 pos) {			
			if( ! isInit ) return null;
			if (! collections.ContainsKey( key ) ){
				print ( "Error : Pool Object Name : " + key );	
				return null;
			}
			return collections[ key ].GetObject( pos );
		}
		
		/// <summary>
		/// Hides the object.
		/// 소모한 object 로써 재사용을 위해 비활성화 시킨다.
		/// </summary>
		/// <param name='doomedObject'>
		/// Doomed object.
		/// </param>
//		public void HideObject( GameObject doomedObject , float time) {	StartCoroutine( HideObjectTimer( doomedObject, time) );	}
//		IEnumerator HideObjectTimer(GameObject doomedObject , float time){
		public void HideObject( GameObject doomedObject ) {	
			
			if (collections.ContainsKey( doomedObject.gameObject.name ) ){
				doomedObject.SetActive(false);
//				doomedObject.transform.parent = transform;
//				transform.position = Vector3.zero;	
			}else if( doomedObject.transform.parent ){
				if (collections.ContainsKey( doomedObject.transform.parent.name ) ){
					doomedObject.transform.parent.gameObject.SetActive(false);
				}
			}
		}
		
		public void AllHideObject(){
			for(int fid = 0; fid < folders.Length; fid++){
				foreach ( GPPrefabInfo info in folders[fid].prefabInfos ){
					GPCollection collection = collections[ info.name ];
					
					foreach( GameObject go in collection.objects ){
						go.SetActive(false);	
					}
				}				
			}
		}
		
		/// <summary>
		/// Hide the specified doomedObject.
		/// 오브젝트를 바로 끌 경우 곧바로 재사용되는 경우가 발생한다.
		/// 이때 해당 오브젝트의 설정 값들이 초기화 처리를 여기서 담당 하지 않는 관계로 변수 값이 유지될수 있다. 
		/// 잠깐의 딜레이를 두어 곧바로 재사용 되지 않게 예외 처리 한다.
		/// </summary>
		/// <param name='doomedObject'>
		/// Doomed object.
		/// </param> 
	//	IEnumerator Hide(GameObject doomedObject){
	//		doomedObject.renderer.enabled = false;
	//		doomedObject.collider.enabled = false;
	//		yield return new WaitForSeconds(2f);
	//		doomedObject.SetActive(false);   
	//	}
		
		
	}//End Class
}
