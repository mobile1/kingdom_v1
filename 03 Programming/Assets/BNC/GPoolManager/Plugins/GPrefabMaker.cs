﻿using UnityEngine;
using System.Collections;

public class GPrefabMaker : MonoBehaviour {
	public GameObject prefab;
	public GameObject linkObject;

	void Awake() {
		linkObject = Instantiate( prefab ) as GameObject;
		linkObject.transform.parent = transform;
		linkObject.transform.localPosition = Vector3.zero;

	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
