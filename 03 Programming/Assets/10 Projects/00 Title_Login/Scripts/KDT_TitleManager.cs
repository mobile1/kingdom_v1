﻿using UnityEngine;
using System.Collections;

public class KDT_TitleManager : MonoBehaviour {
	public GameObject[] titles;

	public UILabel labelMsg;
	public string strMsg;
	public float typingSpeed = 0.1f;

	public GameObject startBt;
	
	public KDS_SceneChanger.SceneID moveSceneID;

	public Animation title;
	public Animation titleMessage;

	public GameObject effect;
	
	// Use this for initialization
	void Start () {
		labelMsg.gameObject.SetActive (false);
		
		startBt.SetActive(false);

		title.gameObject.SetActive(false);
		titleMessage.gameObject.SetActive(false);
		
		StartCoroutine( DoIntro () );
		
	}
	
	
	void AllTitlesOff(){
		foreach( GameObject go in titles ){
			go.SetActive( false );
		}
	}

	IEnumerator DoIntro(){

		yield return new WaitForSeconds( 0f );

		for (int i = 0; i < titles.Length; i++) {
			AllTitlesOff ();
			titles [i].SetActive (true);

			if( i < titles.Length - 1 ) yield return new WaitForSeconds(2f);
		}

		labelMsg.gameObject.SetActive (true);

		strMsg = "업데이트 정보를 확인 중입니다.";

		for( int i = 1; i < strMsg.Length + 1; i++)
		{
			yield return new WaitForSeconds(0.1f);

			labelMsg.text = strMsg.Substring(0, i );
		}

		yield return new WaitForSeconds(1f);

		title.gameObject.SetActive(true);
		title.Play();

		labelMsg.gameObject.SetActive (false);

		yield return new WaitForSeconds(title.clip.length + 0.5f);

//		title.clip.length

		titleMessage.gameObject.SetActive(true);
		titleMessage.Play ();

		startBt.SetActive (true);
	}
	
	public void OnClickGoMainMenu()
	{
		startBt.SetActive(false);
		KDS_SceneChanger.LoadLevel( moveSceneID , false); 

//		effect.layer =  LayerMask.NameToLayer( "InGameUI" );
//		Instantiate( effect, transform.localPosition + new Vector3(0,0,-1.5f), Quaternion.identity );
	}


}
