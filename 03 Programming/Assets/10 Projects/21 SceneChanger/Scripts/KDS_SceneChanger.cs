﻿using UnityEngine;
using System.Collections;

public class KDS_SceneChanger : MonoSingleton<KDS_SceneChanger> {
	public enum SceneID{
		KDTitle,
		KDSceneChanger, 
		KDMainMenu,
		KDInGame,
	}
	public static SceneID willMoveSceneID = SceneID.KDTitle;
	public static bool isQuickChange = false;
	
	public UILabel messageLabel;
	public float typingSpeed = 0.1f;
	public string txtMessage;
	private string txtProgress;
	public bool isLoading;
	// Use this for initialization
	void Start () 
	{
		LoadMessageFromJSon();
		DoLoadingMessage();		
	}
	
	public static void LoadLevel( SceneID _id, bool _isQuick ){
		willMoveSceneID = _id;
		isQuickChange = _isQuick;
		Application.LoadLevel( (SceneID.KDSceneChanger).ToString() );
	}
	
	public void LoadMessageFromJSon()
	{		
		txtMessage = willMoveSceneID.ToString() + "를(을) 로딩 중 입니다.\n\n잠시만 기다려주세요\n";
		txtProgress = "............................";
//		messageLabel.transform.localScale = new Vector3(0.5f,0.5f,1f);		
	}
	
	public void DoLoadingMessage(){ StartCoroutine( DoLoadingMessage( 0f ) ); }
	public IEnumerator DoLoadingMessage( float _time )
	{
		yield return new WaitForSeconds( _time );
		
		if( ! isQuickChange ){
			for( int i = 0; i < txtMessage.Length; i++)
			{
				yield return new WaitForSeconds( typingSpeed );	
				
				messageLabel.text = txtMessage.Substring(0, i );
			}
		}else{
			messageLabel.text = txtMessage;
		}
			
		
		loadingProcessInc = Time.time;
		
		isLoading = true;
//		print ( willMoveSceneName );
//		Application.LoadLevelAsync(  (int) willMoveSceneID );
		print (willMoveSceneID.ToString ());
		Application.LoadLevel( (int) willMoveSceneID );
	}	
	
	float loadingProcessInc = 0f;
	void OnGUI(){
		
		if ( isLoading && Time.time >= loadingProcessInc){
				
			loadingProcessInc = Time.time + typingSpeed;
			
			int subCount = messageLabel.text.Length -  txtMessage.Length;
			
			if( subCount < txtProgress.Length )
			{
				messageLabel.text = txtMessage + txtProgress.Substring(0, subCount + 1 );	
			}
			else
			{
				messageLabel.text = "";	
			}
		}
	}
}
